<?php namespace mef\Stringifier\Example;

require __DIR__ . '/../vendor/autoload.php';

class Test
{
	public $testProperty;
}

// This accepts the same options as json_encode.
$stringifier = new \mef\Stringifier\JsonStringifier(JSON_PRETTY_PRINT);

// The following are exact outputs from json_encode.
echo $stringifier->stringify(['Hello, World!']), PHP_EOL;
echo $stringifier->stringify(['foo' => 'bar']), PHP_EOL;
echo $stringifier->stringify(new Test), PHP_EOL;

// There is an exception for resource... json_encode would return false
$fp = fopen('php://memory', 'r');
echo $stringifier->stringify($fp), PHP_EOL;
fclose($fp);