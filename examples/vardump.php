<?php namespace mef\Stringifier\Example;

require __DIR__ . '/../vendor/autoload.php';

class Test
{
	public $testProperty;
}

// It takes no parameters.
$stringifier = new \mef\Stringifier\VarDumpStringifier;

// The following are exact outputs from var_dump
echo $stringifier->stringify(['Hello, World!']), PHP_EOL;
echo $stringifier->stringify(['foo' => 'bar']), PHP_EOL;
echo $stringifier->stringify(new Test), PHP_EOL;

$fp = fopen('php://memory', 'r');
echo $stringifier->stringify($fp), PHP_EOL;
fclose($fp);