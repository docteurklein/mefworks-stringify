<?php namespace mef\Stringifier\Test;

use \mef\Stringifier\Stringifier;
use \mef\Stringifier\StringifierAwareTrait;
use \mef\Stringifier\StringifierAwareInterface;

class StringifierAwareTraitTest extends \PHPUnit_Framework_TestCase
{
	public function testTrait()
	{
		$foo = new Foo;

		$this->assertNull($foo->getStringifier());

		$stringifier = new Stringifier;
		$foo->setStringifier($stringifier);

		$this->assertSame($stringifier, $foo->getStringifier());
	}
}

class Foo implements StringifierAwareInterface
{
	use StringifierAwareTrait;
}