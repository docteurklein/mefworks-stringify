<?php namespace mef\Stringifier\Test;

use \mef\Stringifier\PrintRStringifier;

class PrintRTest extends \PHPUnit_Framework_TestCase
{
	public function testStringResponse()
	{
		$stringifier = new PrintRStringifier;

		$this->assertTrue(is_string($stringifier->stringify(null)));
		$this->assertTrue(is_string($stringifier->stringify(1)));
		$this->assertTrue(is_string($stringifier->stringify(1.1)));
		$this->assertTrue(is_string($stringifier->stringify('1')));
		$this->assertTrue(is_string($stringifier->stringify(['1'])));
		$this->assertTrue(is_string($stringifier->stringify(['foo' => 'bar'])));
		$this->assertTrue(is_string($stringifier->stringify(new \Stdclass)));
		$this->assertTrue(is_string($stringifier->stringify(fopen('php://memory', 'r'))));
	}
}