<?php namespace mef\Stringifier\Test;

use \mef\Stringifier\JsonStringifier;

class JsonTest extends \PHPUnit_Framework_TestCase
{
	public function testDefaultParameters()
	{
		$json = new JsonStringifier;
		$this->assertSame(0, $json->getOptions());
		$this->assertSame(JsonStringifier::DEFAULT_DEPTH, $json->getDepth());
	}

	public function testConstructorParameters()
	{
		$options = 15;
		$depth = 10;

		$json = new JsonStringifier($options, $depth);
		$this->assertSame($options, $json->getOptions());
		$this->assertSame($depth, $json->getDepth());
	}

	public function testChangeParameters()
	{
		$options = 15;
		$depth = 10;

		$json = new JsonStringifier;

		$json->setOptions($options);
		$json->setDepth($depth);

		$this->assertSame($options, $json->getOptions());
		$this->assertSame($depth, $json->getDepth());
	}

	public function testStringResponse()
	{
		$json = new JsonStringifier;

		$this->assertTrue(is_string($json->stringify(null)));
		$this->assertTrue(is_string($json->stringify(1)));
		$this->assertTrue(is_string($json->stringify(1.1)));
		$this->assertTrue(is_string($json->stringify('1')));
		$this->assertTrue(is_string($json->stringify(['1'])));
		$this->assertTrue(is_string($json->stringify(['foo' => 'bar'])));
		$this->assertTrue(is_string($json->stringify(new \Stdclass)));
		$this->assertTrue(is_string($json->stringify(fopen('php://memory', 'r'))));
	}
}