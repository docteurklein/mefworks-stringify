mef\Stringifier - CHANGELOG
===========================

**Version 1.1.0**, 17-Oct-2014

* Add `StringifierAwareInterface` and `StringifierAwareTrait`
* Add bin/test.sh for automated testing

**Version 1.0.1**, 26-Sep-2014

* Bug fix: don't consider objects with __call as able to be converted to a
string via __toString.

**Version 1.0.0**, 17-Sep-2014

* First release