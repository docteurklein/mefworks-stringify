<?php namespace mef\Stringifier;

use mef\Stringifier\StringifierInterface;

trait StringifierAwareTrait
{
	/**
	 * @var \mef\Stringifier\StringifierInterface
	 */
	protected $stringifier;

	/**
	 * Set the stringifier
	 *
	 * @param \mef\Stringifier\StringifierInterface $stringifier
	 */
	public function setStringifier(StringifierInterface $stringifier)
	{
		$this->stringifier = $stringifier;
	}

	/**
	 * Return the stringifier
	 *
	 * @return \mef\Stringifier\StringifierInterface
	 */
	public function getStringifier()
	{
		return $this->stringifier;
	}
}